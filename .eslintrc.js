module.exports = {
    parserOptions: {
        ecmaVersion: 8,
        sourceType: 'module'
    },
    env: {
        node: true,
        es6: true
    },
    extends: [
        'plugin:prettier/recommended', // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
        'eslint:recommended'
    ],
    rules: {
        'no-unused-vars': 1,
        'no-empty': 1,
        'prettier/prettier': 'warn'
    }
};
