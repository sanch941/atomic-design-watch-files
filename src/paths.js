const path = require('path');

const rootDir = path.join(__dirname, '..');
const envLocation = path.join(rootDir, '.env');

module.exports = {
    rootDir,
    envLocation
};
