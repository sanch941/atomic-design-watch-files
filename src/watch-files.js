const path = require('path');

const chokidar = require('chokidar');

const { WATCH_FILES_MAIN_PATH = 'src' } = process.env || {};

const pathToStore = path.join(WATCH_FILES_MAIN_PATH, 'store');

const { createFilesStore } = require('./watch-files-create');

const watchFilesStore = async (dir) => {
    createFilesStore(dir);
};

const baseSettings = {
    depth: 0,
    ignoreInitial: true
};

module.exports.run = () => {
    // One-liner for current directory
    chokidar.watch(pathToStore, baseSettings).on('addDir', (event) => {
        watchFilesStore(event);
    });
};
