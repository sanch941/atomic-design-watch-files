const camelize = (str, shouldUppercase) => {
    const arr = str.split('-');
    const strPartUppercase = (part) =>
        part.charAt(0).toUpperCase() + part.slice(1).toLowerCase();

    const isFirstPartUppercase = (part) =>
        shouldUppercase ? strPartUppercase(part) : part;

    const capital = arr.map((item, index) =>
        index ? strPartUppercase(item) : isFirstPartUppercase(item)
    );
    // ^-- change here.
    const capitalString = capital.join('');
    return capitalString;
};

const getIsReactNative = () => global.TARGET_ENV === 'react-native';

module.exports = {
    camelize,
    getIsReactNative
};
