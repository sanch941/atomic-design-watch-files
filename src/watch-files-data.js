const { camelize, getIsReactNative } = require('./lib');
const isReactNative = getIsReactNative();

const storeActions = (componentName) => {
    const camelized = camelize(componentName, true);
    const camilezedLower = camelize(componentName);

    return `import { InferActionsTypes, BaseThunkType } from '../store';
    
export const TEST_CONSTANT = 'TEST_CONSTANT';

const actions = {
    testAction: (bool: boolean) =>
        ({
            type: TEST_CONSTANT,
            payload: bool
        } as const)
};

export const ${camilezedLower}Actions = actions;

export type ${camelized}Actions = InferActionsTypes<typeof actions>;
type ThunkType = BaseThunkType<${camelized}Actions>;

const ${camilezedLower}Action = (bool: boolean): ThunkType => (dispatch) => {
    dispatch(actions.testAction(bool));
};

export const ${camilezedLower}ThunkActions = {
    ${camilezedLower}Action
};
`;
};

const storeReducer = (componentName) => {
    const camelized = camelize(componentName, true);
    const camilezedLower = camelize(componentName);

    return `import { ${camelized}Actions } from './actions';

interface InitialState {} 

const initialState: InitialState = {};

export const ${camilezedLower}Reducer = (
    state = initialState,
    action: ${camelized}Actions
): InitialState => {
    switch (action.type) {
        default:
            return state;
    }
};
`;
};

const atomicCommonCode = (folderName, isTemplate) => {
    const templateStr = isTemplate ? 'Template' : '';
    return !isReactNative
        ? `import React, { FC } from 'react';
import styled from 'styled-components';

export const ${camelize(folderName, true)}${templateStr}: FC = () => {
    return (
        <StyledContainer>
            <div />
        </StyledContainer>
    )
}

const StyledContainer = styled.div\`
    position: relative;
\`    
`
        : `import React, { FC } from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';

export const ${camelize(folderName, true)}${templateStr}: FC = () => {
    return (
        <StyledContainer>
            <View />
        </StyledContainer>
    )
}

const StyledContainer = styled.View\`
    position: relative;
\`
`;
};

const atomicTemplateCode = (folderName) => atomicCommonCode(folderName, true);

const getDataForComponents = () => [];

const dataForStore = [
    { filename: 'actions.ts', content: storeActions },
    { filename: 'reducer.ts', content: storeReducer }
];

module.exports = {
    atomicCommonCode,
    atomicTemplateCode,
    dataForStore,
    getDataForComponents
};
