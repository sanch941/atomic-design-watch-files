const fs = require('fs');
const path = require('path');
const { run } = require('./watch-files');
const { atomicCommonCode, atomicTemplateCode } = require('./watch-files-data');
const mkdirp = require('mkdirp');
const { camelize } = require('./lib');

const atomicItems = ['organism', 'molecule', 'atom', 'template'];

const linkWithIndex = ({
    isInAtomicDirectory,
    currentDir,
    filename,
    option
}) => {
    const idxDir = isInAtomicDirectory
        ? path.join(currentDir, '..')
        : currentDir;
    const idxLocation = path.join(idxDir, 'index.ts');
    const camelizedFileName = camelize(filename, true);
    const fileNameExported =
        option === 'template'
            ? `${camelizedFileName}Template`
            : camelizedFileName;
    // const dirName = camelize(idxDir.split(/\/|\\/).pop(), true);

    fs.appendFileSync(
        idxLocation,
        `export { ${fileNameExported} } from './${option}s/${filename}'; \n`
    );
};

const getDirectory = (option, filename) => {
    const currentDir = process.cwd();
    const isInAtomicDirectory = atomicItems.some((atomicItem) =>
        currentDir.includes(atomicItem)
    );
    const atomicDir = path.join(currentDir, `${option}s`);

    linkWithIndex({ isInAtomicDirectory, currentDir, filename, option });

    if (!isInAtomicDirectory) {
        mkdirp.sync(atomicDir);

        return atomicDir;
    }

    return '';
};

const cli = (options) => {
    if (options.watch) {
        console.log('watching for folder changes');
        return run();
    }

    Object.keys(options).forEach((optionKey) => {
        const isAtomicItem = atomicItems.includes(optionKey);
        const option = options[optionKey];
        const getContent = (filename) =>
            optionKey === 'template'
                ? atomicTemplateCode(filename)
                : atomicCommonCode(filename);

        if (option && isAtomicItem) {
            option.map((filename) => {
                const directory = getDirectory(optionKey, filename);
                const filenamePath = path.join(directory, `${filename}.tsx`);

                fs.writeFileSync(filenamePath, getContent(filename));
            });
        }
    });
};

module.exports = {
    cli
};
