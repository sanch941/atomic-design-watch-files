const fs = require('fs').promises;
const path = require('path');
const { getIsReactNative } = require('./lib');
const { getDataForComponents, dataForStore } = require('./watch-files-data');

const isReactNative = getIsReactNative();

const getFilePath = (dir) => {
    const componentName = dir.split(/\/|\\/).pop();
    return (name) => {
        const getFilePath = () => {
            if (name === 'container.tsx') {
                return path.join(dir, `${componentName}.tsx`);
            }

            return path.join(dir, `${name}`);
        };

        return {
            filePath: getFilePath(),
            componentName
        };
    };
};

const createFilesComponents = async (dir) => {
    try {
        const setFilePath = getFilePath(dir);
        const pathForFreeName = getFilePath(dir, true);
        const data = getDataForComponents({ isReactNative });

        for (const item of data) {
            if (!item.lock) {
                const { filePath, componentName } = setFilePath(
                    item.fileName,
                    item.divider
                );
                const content = item.content(componentName);
                await fs.writeFile(filePath, content);
            }
        }

        const { filePath: pathForIndex } = pathForFreeName('index.ts', true);
        await fs.writeFile(pathForIndex, ``);
    } catch (err) {
        console.log(err);
    }
};

const createFilesStore = async (dir) => {
    try {
        const setFilePath = getFilePath(dir);
        const pathForFreeName = getFilePath(dir, true);

        for (const item of dataForStore) {
            const { filePath, componentName } = setFilePath(
                item.filename,
                item.divider
            );

            const content = item.content(componentName);
            await fs.writeFile(filePath, content);
        }

        const { filePath: pathForIndex } = pathForFreeName('index.ts', true);
        fs.writeFile(
            pathForIndex,
            `export * from './reducer';
export * from './actions';
`
        );
    } catch (err) {
        console.log(err);
    }
};

module.exports.createFilesComponents = createFilesComponents;
module.exports.createFilesStore = createFilesStore;
