import { MemActions } from './actions';

interface InitialState {} 

const initialState: InitialState = {};

export const memReducer = (
    state = initialState,
    action: MemActions
): InitialState => {
    switch (action.type) {
        default:
            return state;
    }
};
