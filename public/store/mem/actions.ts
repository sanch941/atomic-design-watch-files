import { InferActionsTypes, BaseThunkType } from '../store';
import { TEST_CONSTANT } from './constants';

const actions = {
    testAction: (bool: boolean) =>
        ({
            type: TEST_CONSTANT,
            payload: bool
        } as const)
};

export type MemActions = InferActionsTypes<typeof actions>;
type ThunkType = BaseThunkType<MemActions>;

const memAction = (bool: boolean): ThunkType => (dispatch) => {
    dispatch(actions.testAction(bool));
};

export const memThunkActions = {
    memAction
};
