#!/usr/bin/env node
require('dotenv').config();

global.TARGET_ENV = '';

const arg = require('arg');

const parseArguments = (rawArgs) => {
    try {
        const args = arg(
            {
                '--atom': [String],
                '-a': '--atom',

                '--molecule': [String],
                '-m': '--molecule',

                '--organism': [String],
                '-o': '--organism',

                '--template': [String],
                '-t': '--template',

                '--watch': Boolean,
                '-w': '--watch',
                '--r-native': Boolean,
                '-r': '--r-native'
            },
            {
                argv: rawArgs.slice(2)
            }
        );
        return {
            organism: args['--organism'] || false,
            molecule: args['--molecule'] || false,
            atom: args['--atom'] || false,
            template: args['--template'] || false,
            watch: args['--watch'] || false,
            reactnative: args['--r-native'] || false
        };
    } catch (err) {
        err.message && console.log(err.message);
    }
};

const options = parseArguments(process.argv);

if (options.reactnative) {
    global.TARGET_ENV = 'react-native';
}

const { cli } = require('../src/cli');
cli(options);
